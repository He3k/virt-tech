#! /bin/bash

docker build -t test_gcc:build . # builder
docker run --rm --name test_gcc -p 80:8080 test_gcc:build # runner

docker create --name extract test_gcc:build
mkdir ./Production/
docker cp extract:/go/main ./Production/main
docker rm -f extract
docker rm -f hello_world
#! /bin/bash

echo "Add user 1"
curl -X PUT "http://0.0.0.0:5000/users" -H "Content-Type: application/json" -d '{"name": "John Doe", "email": "johndoe@example.com"}'
echo "Add user 2"
curl -X PUT "http://0.0.0.0:5000/users" -H "Content-Type: application/json" -d '{"name": "nick", "email": "nicko@example.com"}'
echo "Add user 3"
curl -X PUT "http://0.0.0.0:5000/users" -H "Content-Type: application/json" -d '{"name": "serega", "email": "serega@example.com"}'
echo "List users"
curl -X GET "http://0.0.0.0:5000/users"
echo "User 1"
curl -X GET "http://0.0.0.0:5000/users/1"
echo "User 2"
curl -X GET "http://0.0.0.0:5000/users/2"
echo "User 3"
curl -X GET "http://0.0.0.0:5000/users/3"
echo "Delete user 1"
curl -X DELETE "http://0.0.0.0:5000/users/1"
echo "List users"
curl -X GET "http://0.0.0.0:5000/users"

from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)

# Конфигурация базы данных
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# Описание модели User
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    email = db.Column(db.String(50))

    def to_json(self):
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email
        }

# Эндпоинт для получения списка пользователей с пагинацией
@app.route('/users', methods=['GET'])
def get_users():
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', 10, type=int)
    pagination = User.query.paginate(page=page, per_page=per_page, error_out=False)
    users = pagination.items
    return jsonify([user.to_json() for user in users]), 200

# Эндпоинт для создания пользователя
@app.route('/users', methods=['PUT'])
def create_user():
    data = request.get_json()
    user = User(name=data['name'], email=data['email'])
    db.session.add(user)
    db.session.commit()
    return jsonify(user.to_json()), 201

# Эндпоинт для получения пользователя по ID
@app.route('/users/<int:user_id>', methods=['GET'])
def get_user(user_id):
    user = User.query.get_or_404(user_id)
    return jsonify(user.to_json())

# Эндпоинт для удаления пользователя
@app.route('/users/<int:user_id>', methods=['DELETE'])
def delete_user(user_id):
    user = User.query.get_or_404(user_id)
    db.session.delete(user)
    db.session.commit()
    return jsonify({ 'message': 'User deleted.' }), 200

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
